from math import *

class Vector():
    def __init__(self,x=None,y=None,z=None):
        if type(x)==list and y==None and z==None:
            try:
                self.__x=float(x[0])
                self.__y=float(x[1])
                self.__z=float(x[2])
            except :
                self.__isError()
        elif x==None or y==None or z==None:
            self.__isError()
        else:
            try:
                self.__x=float(x)
                self.__y=float(y)
                self.__z=float(z)
                self.__Length()
            except TypeError:
                self.__isError()

    def __str__(self):
        string=self.__x,self.__y,self.__z
        return str(string)

    def __isError(self):
        self.__x=0
        self.__y=0
        self.__z=0
        self.__length=0

    def __Length(self):
        self.__length=sqrt((self.__x**2)+(self.__y**2)+(self.__z**2))

    def getX(self):
        return self.__x

    def setX(self,x):
        try:
            self.__x=float(x)
        except:
            return self

    def getY(self):
        return self.__y

    def setY(self,y):
        try:
            self.__y=float(y)
        except:
            return self

    def getZ(self):
        return self.__z

    def setZ(self,z):
        try:
            self.__z=float(z)
        except:
           return self

    def getLength(self):
        return self.__length

    def __add__(self,vector):
        if type(vector)==Vector:
            x=self.__x+vector.getX()
            y=self.__y+vector.getY()
            z=self.__z+vector.getZ()
            return Vector(x,y,z)


    def __sub__(self,vector):
        if type(vector)==Vector:
            x=self.__x-vector.getX()
            y=self.__y-vector.getY()
            z=self.__z-vector.getZ()
            return Vector(x,y,z)


    def __mul__(self,number):
        try:
            x=self.__x*float(number)
            y=self.__y*float(number)
            z=self.__z*float(number)
            return (x,y,z)
        except:
            return self

    def __rmul__(self, number):
        try:
            x=float(number)*self.__x
            y=float(number)*self.__y
            z=float(number)*self.__z
            return (x,y,z)
        except:
            return self

    def scalar(self,vector):
        scalar=self.__x*vector.getX()+self.__y*vector.getY()+self.__z*vector.getZ()
        return scalar

    def mul(self, vector):
        x=self.__x*vector.getZ()-self.__z*vector.getX()
        y=self.__y*vector.getZ()-self.__z*vector.getY()
        z=self.__x*vector.getY()-self.__y*vector.getX()
        return Vector(x,y,z)

    def angleX(self):
        if self.__length!=0:
            cosX=acos(self.__x/self.__length)
            return cosX
    def angleY(self):
        if self.__length!=0:
            return  self.__y/self.__length
    def angleZ(self):
        if self.__length!=0:
            return  self.__z/self.__length

class A:
    def __init__(self):
        self.a=10
a=A()

k=[1,2,3]
v=Vector(3,4,5)

v1=Vector(-1,-12,1)

print('v=',v)
print('v1=',v1)
print('v+v1=',v+v1)
print('v.mul(v1)=',v.mul(v1))
